//
//  ViewController.swift
//  ExampleLocalExercise
//
//  Created by Milo Winningham on 1/6/16.
//  Copyright © 2016 Treehouse. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didClickButton(sender: AnyObject) {
        textField.text = ExampleModel.exampleString()
    }
    
    @IBOutlet weak var textField: UITextField!
}

