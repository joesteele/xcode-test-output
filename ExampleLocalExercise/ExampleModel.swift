//
//  ExampleModel.swift
//  ExampleLocalExercise
//
//  Created by Milo Winningham on 1/7/16.
//  Copyright © 2016 Treehouse. All rights reserved.
//

import Foundation

class ExampleModel {
    static func exampleString() -> String {
        return "example!"
    }

    static func otherExampleString() -> String {
        return "example!"
    }
}
