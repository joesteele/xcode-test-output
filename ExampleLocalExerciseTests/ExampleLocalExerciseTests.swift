//
//  ExampleLocalExerciseTests.swift
//  ExampleLocalExerciseTests
//
//  Created by Milo Winningham on 1/6/16.
//  Copyright © 2016 Treehouse. All rights reserved.
//

import XCTest
@testable import ExampleLocalExercise

class ExampleLocalExerciseTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExampleString() {
        XCTAssert(ExampleModel.exampleString() == "example!", "exampleString() should equal 'example!'")
    }
    
    func testOtherExampleString() {
        XCTAssert(ExampleModel.otherExampleString() == "other", "otherExampleString() should equal 'other'")
    }
    
}
