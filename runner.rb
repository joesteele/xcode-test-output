require "json"
require "open3"
require "pp"
require "xcpretty"

class Runner
  def initialize(sdk: "iphonesimulator", destination: "platform=iOS Simulator,name=iPhone 6,OS=9.2", scheme:)
    @cmd = ["xcodebuild", "-sdk", sdk, "-destination", destination, "-scheme", scheme, "test"]
  end

  attr_reader :parser

  def running?
    @thread && @thread.alive?
  end

  def success?
    @thread && !@thread.alive? && @thread.value.exitstatus == 0
  end

  def run
    @input, @output, @thread = Open3.popen2e(*@cmd)
    @parser = Parser.new

    while line = @output.gets
      @parser << line
    end

    @input.close
    @output.close
  end
end

class Parser
  include XCPretty::Matchers

  def initialize
    @started = false
    @compiled = false

    @passes = []
    @fails = []
  end

  attr_reader :passes, :fails

  def started?; @started; end
  def compiled?; @compiled; end

  def <<(line)
    case line
    when BUILD_TARGET_MATCHER
      @started = true
    when TESTS_RUN_START_MATCHER
      @compiled = true
    when PASSING_TEST_MATCHER
      @passes << { suite: $1, case: $2, time: $3.to_f }
    when FAILING_TEST_MATCHER
      @fails  << { file: $1, suite: $2, case: $3, message: $4 }
    end
  end
end

if __FILE__ == $0
  r = Runner.new(scheme: "ExampleLocalExercise")
  r.run
  puts JSON.pretty_generate(passes: r.parser.passes, fails: r.parser.fails)
end
